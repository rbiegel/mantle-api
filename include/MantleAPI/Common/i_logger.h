/*******************************************************************************
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef MANTLEAPI_COMMON_I_LOGGER_H
#define MANTLEAPI_COMMON_I_LOGGER_H

#include <string_view>

namespace mantle_api
{

/// Log levels for the logging interface
enum class LogLevel : int
{
  kTrace = 0,  ///< Step by step execution messages for intensive debugging
  kDebug,      ///< Messages considered to be useful for debugging
  kInfo,       ///< Purely informational messages, can be ignored during normal operation
  kWarning,    ///< Messages informing about unexpected behaviour, key business functionality is not impaired
  kError,      ///< Some functionality is not working, key business functionality is not impaired
  kCritical    ///< Key business functionality is not working, the overall system is impaired
};

/// Interface for logging messages
class ILogger
{
public:
  virtual ~ILogger() = default;

  /// Get the current log level from the logging interface
  /// \return The current log level
  [[nodiscard]] virtual LogLevel GetCurrentLogLevel() const noexcept = 0;

  /// Log a message on the logging interface
  /// \param[in] level The log level of the message
  /// \param[in] message The log message
  virtual void Log(LogLevel level, std::string_view message) noexcept = 0;

  /// Log a message with log level "Trace"
  /// \param[in] message The log message
  inline void Trace(std::string_view message) noexcept
  {
    Log(LogLevel::kTrace, message);
  }

  /// Log a message with log level "Debug"
  /// \param[in] message The log message
  inline void Debug(std::string_view message) noexcept
  {
    Log(LogLevel::kDebug, message);
  }

  /// Log a message with log level "Info"
  /// \param[in] message The log message
  inline void Info(std::string_view message) noexcept
  {
    Log(LogLevel::kInfo, message);
  }

  /// Log a message with log level "Warning"
  /// \param[in] message The log message
  inline void Warning(std::string_view message) noexcept
  {
    Log(LogLevel::kWarning, message);
  }

  /// Log a message with log level "Error"
  /// \param[in] message The log message
  inline void Error(std::string_view message) noexcept
  {
    Log(LogLevel::kError, message);
  }

  /// Log a message with log level "Critical"
  /// \param[in] message The log message
  inline void Critical(std::string_view message) noexcept
  {
    Log(LogLevel::kCritical, message);
  }
};

}  // namespace mantle_api

#endif  // MANTLEAPI_COMMON_I_LOGGER_H
