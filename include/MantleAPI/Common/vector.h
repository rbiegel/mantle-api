/*******************************************************************************
 * Copyright (c) 2021, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

//-----------------------------------------------------------------------------
/** @file  vector.h */
//-----------------------------------------------------------------------------

#ifndef MANTLEAPI_COMMON_VECTOR_H
#define MANTLEAPI_COMMON_VECTOR_H

#include <MantleAPI/Common/floating_point_helper.h>

#include <cstdint>

namespace mantle_api
{
template <typename T, class = typename std::enable_if_t<units::traits::is_unit_t<T>::value>>
struct Vec3
{
  Vec3() = default;

  Vec3(T x_in, T y_in, T z_in)
      : x{x_in}, y{y_in}, z{z_in}
  {
  }

  T x{0};
  T y{0};
  T z{0};

  inline T Length() const { return units::math::sqrt((x * x) + (y * y) + (z * z)); }

  inline Vec3<T> operator-() const noexcept
  {
    return {-x, -y, -z};
  }
};

template <typename T>
inline bool operator==(const Vec3<T>& lhs, const Vec3<T>& rhs) noexcept
{
  return IsEqual(lhs.x, rhs.x) && IsEqual(lhs.y, rhs.y) && IsEqual(lhs.z, rhs.z);
}

template <typename T>
inline bool operator!=(const Vec3<T>& lhs, const Vec3<T>& rhs) noexcept
{
  return !(lhs == rhs);
}

template <typename T>
inline Vec3<T> operator-(const Vec3<T>& lhs, const Vec3<T>& rhs) noexcept
{
  return {lhs.x - rhs.x, lhs.y - rhs.y, lhs.z - rhs.z};
}

template <typename T>
inline Vec3<T> operator+(const Vec3<T>& lhs, const Vec3<T>& rhs) noexcept
{
  return {lhs.x + rhs.x, lhs.y + rhs.y, lhs.z + rhs.z};
}

template <typename T>
inline Vec3<T> operator*(const Vec3<T>& lhs, double d) noexcept
{
  return {lhs.x * d, lhs.y * d, lhs.z * d};
}

template <typename T>
inline Vec3<T> operator*(double d, const Vec3<T>& rhs) noexcept
{
  return rhs * d;
}

template <typename T>
inline Vec3<T> operator/(const Vec3<T>& lhs, double d) noexcept
{
  return {lhs.x / d, lhs.y / d, lhs.z / d};
}

template <typename T>
inline Vec3<T> operator+=(Vec3<T>& lhs, const Vec3<T>& rhs) noexcept
{
  lhs.x += rhs.x;
  lhs.y += rhs.y;
  lhs.z += rhs.z;
  return lhs;
}

template <typename T>
inline Vec3<T> operator-=(Vec3<T>& lhs, const Vec3<T>& rhs) noexcept
{
  lhs.x -= rhs.x;
  lhs.y -= rhs.y;
  lhs.z -= rhs.z;
  return lhs;
}

template <typename T>
inline Vec3<T> operator+=(Vec3<T>& lhs, double d) noexcept
{
  lhs.x += d;
  lhs.y += d;
  lhs.z += d;
  return lhs;
}
template <typename T>
inline Vec3<T> operator-=(Vec3<T>& lhs, double d) noexcept
{
  lhs.x -= d;
  lhs.y -= d;
  lhs.z -= d;
  return lhs;
}

}  // namespace mantle_api

#endif  // MANTLEAPI_COMMON_VECTOR_H
