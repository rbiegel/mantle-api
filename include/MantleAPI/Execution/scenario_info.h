/*******************************************************************************
 * Copyright (c) 2021, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

//-----------------------------------------------------------------------------
/** @file  scenario_info.h */
//-----------------------------------------------------------------------------

#ifndef MANTLEAPI_EXECUTION_SCENARIO_INFO_H
#define MANTLEAPI_EXECUTION_SCENARIO_INFO_H

#include <MantleAPI/Common/time_utils.h>

#include <map>
#include <string>

namespace mantle_api
{
struct ScenarioInfo
{
  Time scenario_timeout_duration;
  std::string description;
  std::map<std::string, std::string> additional_information;
};

}  // namespace mantle_api

#endif  // MANTLEAPI_EXECUTION_SCENARIO_INFO_H
