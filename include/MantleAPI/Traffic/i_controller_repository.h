#ifndef MANTLEAPI_TRAFFIC_I_CONTROLLER_REPOSITORY_H
#define MANTLEAPI_TRAFFIC_I_CONTROLLER_REPOSITORY_H

/********************************************************************************
 * Copyright (c) 2021 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
/** @file  i_controller_repository.h */
//-----------------------------------------------------------------------------

#pragma once

#include "MantleAPI/Traffic/i_controller_config.h"
#include "MantleAPI/Traffic/i_controller.h"

namespace mantle_api
{
/// This interface provides CRUD functionality for controllers.
class IControllerRepository
{
public:
  virtual IController& Create(std::unique_ptr<IControllerConfig> config) = 0;
  [[deprecated]] virtual IController& Create(UniqueId id, std::unique_ptr<IControllerConfig> config) = 0; // deprecated

  virtual std::optional<std::reference_wrapper<IController>> Get(UniqueId id) = 0;
  [[nodiscard]] virtual bool Contains(UniqueId id) const = 0;

  virtual void Delete(UniqueId id) = 0;

};

}  // namespace mantle_api
#endif
